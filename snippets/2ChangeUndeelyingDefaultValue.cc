// ********************* 
// *** tcp layer ***
// *********************
Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue (300000000));  		//300M
//? 似乎没必要设置rcv buffer
Config::SetDefault ("ns3::TcpSocket::RcvBufSize", UintegerValue (300000000));  		//300M
Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(1448));


// ********************* 
// ***physical layer ***
// *********************
// drop for disc or device queue is not sure
Config::SetDefault ("ns3::DropTailQueue<Packet>::MaxSize", StringValue ("80p"));
// [check done] 把device queue的大小都设置成这么多 [原本默认是100p] 
Config::SetDefault ("ns3::QueueBase::MaxSize", StringValue ("123p"));
// [check done] 设置qdsic的max length [原本默认是1000p] -- 虽然是继承queueBase的,但是默认值不一样(估计是工厂模式的生成时的设置问题)
TrafficControlHelper tch;
tch.SetRootQueueDisc ("ns3::PfifoFastQueueDisc", "MaxSize", StringValue ("456p"));  
    
//*  查看上述两个的设置成功与否代码
for(uint32_t i =1;i< NodeList::GetNNodes();i++){
    Ptr<Node> node = NodeList::GetNode (i);
    Ptr<TrafficControlLayer> tc = node->GetObject<TrafficControlLayer> ();
    std::map<Ptr<NetDevice>, NetDeviceInfo> devicesInfo = tc->GetNetDevices();

    for(auto iter = devicesInfo.begin();iter!=devicesInfo.end();iter++ ){  //! device's index starts from 0

        uint32_t device_idx = distance(devicesInfo.begin(),iter);
        Ptr<QueueDisc> qdisc = iter->second.m_rootQueueDisc;
        Ptr<PointToPointNetDevice> ptpnd = DynamicCast<PointToPointNetDevice>(iter->first);
        Ptr<Queue<Packet> > queue = ptpnd->GetQueue ();
         //* check config 
             //* get 0 cause we only has pfifo_fast
         // NS_LOG_INFO ( "("<< device_idx <<" @ "<< i <<") qdisc size: " << iter->second.m_queueDiscsToWake.size());
        NS_LOG_INFO ( "("<< device_idx <<" @ "<< i <<") root qdisc packet size: " << iter->second.m_rootQueueDisc->GetMaxSize());
        NS_LOG_INFO ( "("<< device_idx <<" @ "<< i <<") device queue packet size: " << queue->GetMaxSize());
       }
   }
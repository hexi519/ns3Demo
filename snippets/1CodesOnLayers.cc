// ********************* 
// *** NS_LOG ***
// *********************
// [more details@hesy](https://blog.csdn.net/Hesy_H/article/details/105386425)

//* 代码文件内
LogComponentEnable("geaTopo", LOG_LEVEL_INFO); //会显示该level及以上的
LogComponentEnable ("geaConfig", LOG_INFO);    //只会显示该等级

//* 命令行
// enable all
NS_LOG="*" ./waf --run xxx  
// enable specified component and specified level
NS_LOG="test=level_info" ./waf --run xxx
// add prefix
NS_LOG="test=info|prefix_level" ./waf --run xxx
// integrate all
NS_LOG="<log-component>=<option>|<option>...:<log-component>..." ./waf --run xxx
// integrate with gdb    
NS_LOG="*=error:test=debug" ../waf --run myTopo --command-template="gdb --args %s ns3::PointToPointNetDevice::DataRate=10Mbps"
//integrate with valgrind
NS_LOG="*=error:test=debug" ../waf --run myTopo --command-template "valgrind -v --log-file=./valgrind_log"

//* 采用环境变量的方式
// 详情见最上 more details@hesy

// ********************* 
// *** Schedule ***
// *********************
// schedule的时间参数代表的是**延迟**，而不是别的
// scheduleNow是立马开始的，但是，也是遵从FIFO的(毕竟同一时刻可能有多个事件安排生成) 
// 当回调函数是类的成员函数时，需要传入类的对象
Simulator::Schedule(Seconds(5), &MyApp::checkStop,this);  // schedule call class member function

// ********************* 
// *** [链路层] tc层次 ***
// *********************
//* tc层默认是有tc control layer 但是没有queue disc


// ********************* 
// *** [网络层] ip层次 ***
// *********************
//* 可以通过Ipv4L3Protocol对象获取ip，但，device的ipv4Interface，第0个索引的ip是回环地址(127.0.0.1)，获取的时候需要避开
Ipv4Address getRandomIpFromNode(uint32_t NodeId) {
  Ptr<Node> node = NodeList::GetNode(NodeId);
  Ptr<Ipv4L3Protocol> l3p = node->GetObject<Ipv4L3Protocol>();
  int interfaceNum = l3p->GetNInterfaces() - 1;
  uint32_t randomIdx = rand()%( interfaceNum ) + 1;   //!0 is loopback = = need avoid
  Ipv4Address ipAddress = l3p->GetInterface( randomIdx )->GetAddress(0).GetLocal();    
  assert( ipAddress != Ipv4Address("127.0.0.1") );
  return ipAddress;
}


// ********************* 
// *** [应用层] ***
// *********************
//* application的setStartTime(Time)，这个调用了Simulator::Schedule,也就是说这个Time设置的是当前时间的延迟时间
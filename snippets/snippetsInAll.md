# 修改底层的默认值设置
## tcp layer
```c++
Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue (300000000));  		//300M
//? 似乎没必要设置rcv buffer
Config::SetDefault ("ns3::TcpSocket::RcvBufSize", UintegerValue (300000000));  		//300M
Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(1448));
```
## physical layer
```c++
Config::SetDefault ("ns3::DropTailQueue<Packet>::MaxSize", StringValue ("80p"));
// [check done] 把device queue的大小都设置成这么多 [原本默认是100p] 
Config::SetDefault ("ns3::QueueBase::MaxSize", StringValue ("123p"));
// [check done] 设置qdsic的max length [原本默认是1000p] -- 虽然是继承queueBase的,但是默认值不一样(估计是工厂模式的生成时的设置问题)
TrafficControlHelper tch;
tch.SetRootQueueDisc ("ns3::PfifoFastQueueDisc", "MaxSize", StringValue ("456p"));  
    
//*  查看上述两个的设置成功与否代码
for(uint32_t i =1;i< NodeList::GetNNodes();i++){
    Ptr<Node> node = NodeList::GetNode (i);
    Ptr<TrafficControlLayer> tc = node->GetObject<TrafficControlLayer> ();
    std::map<Ptr<NetDevice>, NetDeviceInfo> devicesInfo = tc->GetNetDevices();

    for(auto iter = devicesInfo.begin();iter!=devicesInfo.end();iter++ ){  //! device's index starts from 0

        uint32_t device_idx = distance(devicesInfo.begin(),iter);
        Ptr<QueueDisc> qdisc = iter->second.m_rootQueueDisc;
        Ptr<PointToPointNetDevice> ptpnd = DynamicCast<PointToPointNetDevice>(iter->first);
        Ptr<Queue<Packet> > queue = ptpnd->GetQueue ();
         //* check config 
             //* get 0 cause we only has pfifo_fast
         // NS_LOG_INFO ( "("<< device_idx <<" @ "<< i <<") qdisc size: " << iter->second.m_queueDiscsToWake.size());
        NS_LOG_INFO ( "("<< device_idx <<" @ "<< i <<") root qdisc packet size: " << iter->second.m_rootQueueDisc->GetMaxSize());
        NS_LOG_INFO ( "("<< device_idx <<" @ "<< i <<") device queue packet size: " << queue->GetMaxSize());
       }
   }
```

# NS_LOG使用

⚠ [more details@hesy](https://blog.csdn.net/Hesy_H/article/details/105386425)

## 代码文件内

```c++
LogComponentEnable("geaTopo", LOG_LEVEL_INFO); //会显示该level及以上的
LogComponentEnable ("geaConfig", LOG_INFO);    //只会显示该等级
```

## 命令行

```c
// enable all
NS_LOG="*" ./waf --run xxx  
// enable specified component and specified level
NS_LOG="test=level_info" ./waf --run xxx
// add prefix
NS_LOG="test=info|prefix_level" ./waf --run xxx
// integrate all
NS_LOG="<log-component>=<option>|<option>...:<log-component>..." ./waf --run xxx
// integrate with gdb    
NS_LOG="*=error:test=debug" ../waf --run myTopo --command-template="gdb --args %s ns3::PointToPointNetDevice::DataRate=10Mbps"
```

采用环境变量的方式，详情见reference



# 分层
## NS_LOG
⚠ [more details@hesy](https://blog.csdn.net/Hesy_H/article/details/105386425)

### 代码文件内

```c++
LogComponentEnable("geaTopo", LOG_LEVEL_INFO); //会显示该level及以上的
LogComponentEnable ("geaConfig", LOG_INFO);    //只会显示该等级
```

### 命令行

```c
// enable all
NS_LOG="*" ./waf --run xxx  
// enable specified component and specified level
NS_LOG="test=level_info" ./waf --run xxx
// add prefix
NS_LOG="test=info|prefix_level" ./waf --run xxx
// integrate all
NS_LOG="<log-component>=<option>|<option>...:<log-component>..." ./waf --run xxx
// integrate with gdb    
NS_LOG="*=error:test=debug" ../waf --run myTopo --command-template="gdb --args %s ns3::PointToPointNetDevice::DataRate=10Mbps"
```

### 采用环境变量的方式
* 详情见reference
* 记住不要configure -d optimized

## Schedule
* schedule的时间参数代表的是**延迟**，而不是别的
* scheduleNow是立马开始的，但是，也是遵从FIFO的(毕竟同一时刻可能有多个事件安排生成) 
* 当回调函数是类的成员函数时，需要传入类的对象
```c++
Simulator::Schedule(Seconds(5), &MyApp::checkStop,this);  // schedule call class member function
```

## [链路层] tc层次
* tc层默认是有tc control layer 但是没有queue disc

## [网络层] ip层次
* 可以通过Ipv4L3Protocol对象获取ip，但，device的ipv4Interface，第0个索引的ip是回环地址(127.0.0.1)，获取的时候需要避开
```c++
Ipv4Address getRandomIpFromNode(uint32_t NodeId) {
  Ptr<Node> node = NodeList::GetNode(NodeId);
  Ptr<Ipv4L3Protocol> l3p = node->GetObject<Ipv4L3Protocol>();
  int interfaceNum = l3p->GetNInterfaces() - 1;
  uint32_t randomIdx = rand()%( interfaceNum ) + 1;   //!0 is loopback = = need avoid
  Ipv4Address ipAddress = l3p->GetInterface( randomIdx )->GetAddress(0).GetLocal();    
  assert( ipAddress != Ipv4Address("127.0.0.1") );
  return ipAddress;
}
```

## [应用层]

* application的setStartTime(Time)，这个调用了Simulator::Schedule,也就是说这个Time设置的是当前时间的延迟时间


# mellicious

## waf的working directory

当然是在根目录 也就是waf所在的目录 (scratch的上一级目录)



# 结合vscode的技巧
## 查找
* exclude : test,aodv,click,bridge,dsdv,flow-monitor,lr-wpan,lte,mesh